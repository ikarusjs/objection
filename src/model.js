const { Model } = require('objection');
const inflector = require('inflector-js')

module.exports = class extends Model {
  static get tableName() {
    return inflector.pluralize(this.name.toLowerCase())
  }

  $beforeInsert() {
    this.createdAt = this.updatedAt = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updatedAt = new Date().toISOString();
  }
}
