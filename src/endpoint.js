const createObjectionService = require('feathers-objection');

module.exports = function({ model, rules = [] }) {
  return app => ({
    service: () => {
      return createObjectionService({ model, paginate: app.get('paginate') })
    },
    afterRegister: ({ service, path }) => {
      for(rule of rules) {
        rule({ service })
      }
    }
  })
}
