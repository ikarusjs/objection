const expect = require('expect.js')
const { app: ikarus } = require('@ikarusjs/ikarus')
const { initializer, Model, endpoint } = require('../src')

describe('objection package', () => {

  describe('initializer', () => {
    it('adds knex to app config', () => {
      const app = ikarus({
        bootstrap: [
          app => {
            app.set('mysql', { client: 'mysql2', connection: 'mysql://root:@localhost:3306/ikarus' })
          },
          initializer()
        ]
      })

      expect(app.get('knex')).to.be.a('function')
    })
  })

  describe('model', () => {
    it('chooses correct table name', () => {
      class Car extends Model {}

      expect(Car.tableName).to.be('cars')
    })
  })

  describe('endpoint', () => {
    it('registers endpoint', () => {
      const test_endpoint = endpoint({
        model: class Car extends Model {},
      })
      const app = ikarus({ endpoints: { '/cars': test_endpoint } })

      expect(app.service('/cars')).to.be.an('object')
    })
  })
})
